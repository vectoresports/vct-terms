jQuery(document).ready(function() {
    jQuery("#vct-terms-accept").click(function() {
        var data = {
            'action': 'vct_terms_accept'
        }

        jQuery.post(ajaxurl, data, function(response) {
            if (response == "true") {
                jQuery("[id=vct-terms]").remove();
            } else {
                alert("Failed to accept. Error has been logged.\n Please try again in a few minutes!");
            }
        });
    });
});
