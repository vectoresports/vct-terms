=== VcT-Terms ===
Contributors: TagnumElite
Tags: vectoresports, games, legal
Requires at least: 4.0
Tested up to: 4.8
Requires PHP: 5.6
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

VcT-Terms is a simple plugin that allows for easy to setup TOC notifications.

== Description ==
Long description of this great plugin. No characters limit, and you can use markdown.

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
markdown parsed.

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Link to [WordPress](http://wordpress.org/ \"Your favorite software\") and one to [Markdown\'s Syntax Documentation][markdown syntax].

Titles are optional, naturally.

Asterisks for *emphasis*.

Double it up  for **strong**.

== Installation ==

= Manual =
1. Upload \"vct-terms\" folder to the \"/wp-content/plugins/\" directory.
1. Activate the plugin through the \"Plugins\" menu in WordPress.

= Auto =
1. Upload \"vct-terms.zip\" through the \"Plugins\" menu in WordPress

= Finally =
Go to General Settings and setup the plugin!

== Frequently Asked Questions ==
= A question that someone might have =
An answer to that question.

= What about foo bar? =
Answer to foo bar dilemma.

== Screenshots ==
1. Options Menu.
2. The Modal: Version 1.0.2 and Below
3. The Modal: Version 1.0.3
4. The Modal: Version 1.1.0 - ~

== Changelog ==
= 1.1.0 =
* Fixed Inline Accept Button
* Add WordPress Editor for info on modal.

= 1.0.3 =
* Improved Modal
* Disable on Customize Preview

= 1.0.2 =
* Added Datetime logging of accept button

= 1.0.0 =
* Initial release.

== Upgrade Notice ==
= 1.1.0 =
* Fixed Inline accept button
* New Modal Info option!

= 1.0.3 =
* Upgrade to get new and improved Modal
* Disabled on Customize Preview

= 1.0.2 =
* Added Datetime logging of accept button

= 1.0.0 =
This version includes a working plugin!