<?php
/**
 * VcT-Terms
 *
 * @package     VcT_Terms
 * @author      TagnumElite
 * @copyright   2018 Vector eSports
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: VcT-Terms
 * Plugin URI:  https://bitbucket.org/vectoresports/vct-terms
 * Description: A tiny plugin that allows for terms and conditions notifications.
 * Version:     1.1.1
 * Author:      Vector eSports
 * Author URI:  https://vectoresports.co.za
 * Text Domain: vct-terms
 * License:     GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 */

class VcT_Terms {
    public $plugin_name = 'vct-terms';

    public $version = "1.1.1";

    public function pretty_name() {
        $name = $this->plugin_name;

        $name = str_replace('-', '_', $name);

        return $name;
    }

    public function __construct() {
        #register_activation_hook(__FILE__, array( $this, 'activate' ));

        add_action( 'init', array($this, 'activate') );
        add_action( 'admin_init', array( $this, 'settings_init' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
        add_action( 'wp_footer', array( $this, 'footer' ) );
        #add_action( 'plugin_loaded', array ( $this, 'update_db' ) );

        add_action( 'wp_ajax_vct_terms_accept', array( $this, 'accept' ) );

        add_post_type_support( 'post', 'excerpt' );
        add_post_type_support( 'page', 'excerpt' );

        $this->load_textdomain();
    }

    public function settings_init() {
        add_settings_section(
            $this->plugin_name . '_settings',
            esc_html__( 'VcT-Terms', $this->plugin_name ),
            array( $this, 'settings_callback' ),
            'general'
        );

        add_settings_field(
            $this->plugin_name . '_info',
            esc_html__( 'Custom Info', $this->plugin_name ),
            array( $this, 'print_info_editor' ),
            'general',
            $this->plugin_name . '_settings',
            array('label_for' => 'Info to override Excerpt')
        );

		add_settings_field(
			$this->plugin_name . '_term-page',
			#$this->plugin_name . '_page',
			esc_html__( 'Term Page', $this->plugin_name ),
			array( $this, 'print_term_page_field' ),
			'general',
			$this->plugin_name . '_settings'
		);

		add_settings_field(
			$this->plugin_name . '_term-date',
			esc_html__( 'Date', $this->plugin_name ),
			array( $this, 'print_term_date_field' ),
			'general',
			$this->plugin_name . '_settings'
		);

		add_settings_field(
			$this->plugin_name . '_dismissible',
			esc_html__( 'Dismissible', $this->plugin_name ),
			array( $this, 'print_dismissible_field' ),
			'general',
			$this->plugin_name . '_settings'
		);

		add_settings_field(
			$this->plugin_name . '_404',
			esc_html__( 'Display on 404 Page', $this->plugin_name ),
			array( $this, 'print_404_field' ),
			'general',
			$this->plugin_name . '_settings'
		);

		register_setting( 'general', $this->plugin_name . '_info' );
		register_setting( 'general', $this->plugin_name . '_term-page' );
		#register_setting( 'general', $this->plugin_name . '_page' );
		register_setting( 'general', $this->plugin_name . '_term-date' );
		register_setting( 'general', $this->plugin_name . '_dismissible' );
		register_setting( 'general', $this->plugin_name . '_404' );
    }

    public function settings_callback() {
        esc_html_e( 'VcT-Terms Settings', $this->plugin_name );
    }

    public function print_info_editor() {
        $value = get_option( $this->plugin_name . '_info' );

        wp_editor($value, 'vct-terms', array('textarea_name' => $this->plugin_name . '_info'));
    }

	public function print_term_page_field() {
		$value = get_option( $this->plugin_name . '_term-page' );
		#$value = get_option( $this->plugin_name . '_page' );

		echo '<input type="text" name="vct-terms_term-page" value="' . esc_attr( $value ) . '" style="width:300px;margin-right:10px;" />';
		#wp_dropdown_pages(array(
        #    'selected' => $value
        #));
	}

    public function print_term_date_field() {
        $value = get_option( $this->plugin_name . '_term-date' );

		echo '<input type="date" name="vct-terms_term-date" value="' . esc_attr( $value ) . '" style="width:300px;margin-right:10px;" />';
    }

    public function print_dismissible_field() {
        $value = get_option( $this->plugin_name . '_dismissible' );

		echo '<input type="checkbox" name="vct-terms_dismissible" value="' . esc_attr( $value ) . '" style="margin-right:10px;" ' . checked(true, $value, false) . ' />';
    }

    public function print_404_field() {
        $value = get_option( $this->plugin_name . '_404' );

		echo '<input type="checkbox" name="vct-terms_404" value="' . esc_attr( $value ) . '" style="margin-right:10px;" ' . checked(true, $value, false) . ' />';
    }

    public function activate() {
        global $wpdb;
        global $vct_terms_table_name;

        $vct_terms_table_name = $wpdb->prefix . $this->pretty_name() . '_users';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $vct_terms_table_name (
            id int(20) NOT NULL AUTO_INCREMENT,
            user_id int(20) NOT NULL,
            accepted date NOT NULL,
            date datetime NOT NULL,
            ip text NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate; ";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        add_option( $this->plugin_name . '_db_version', $this->version);
    }

    public function update_db() {
        global $wpdb;
        $installed_ver = get_option($this->plugin_name . '_db_version');

        if ($installed_ver != $this->version) {
            $vct_terms_table_name = $wpdb->prefix . $this->pretty_name() . '_users';

            $sql = "CREATE TABLE $vct_terms_table_name (
                id int(20) NOT NULL AUTO_INCREMENT,
                user_id int(20) NOT NULL,
                accepted datetime NOT NULL,
                ip text NOT NULL,
                PRIMARY KEY (id)
            );";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            update_option( $this->plugin_name . '_db_version', $this->version );
        }
    }

    public function get_url() {
        $http = '';
        switch (is_ssl()) {
            case true:
                $http="https://";
                break;
            default:
                $http="http://";
                break;
        }

        $url=$http.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        return $url;
    }

    public function is_term_page() {
        $value = get_option($this->plugin_name.'_term-page', '');

        if ($value == '') {
            return false;
        } elseif ($value == $this->get_url()) {
            return false;
        } elseif (is_customize_preview()) {
            return false;
        } else {
            return true;
        }
    }

    public function enqueue_scripts() {
        if ($this->is_term_page()) {
            wp_enqueue_style( 'vct-terms', plugin_dir_url(__FILE__) . 'vct-terms.css', array(), $this->version );

            wp_enqueue_script( 'vct-terms', plugin_dir_url(__FILE__) . 'vct-terms.js', array(), $this->version, true );
        }
    }

    public function footer() {
        if (is_user_logged_in() and !is_admin()){
            global $wpdb;
            global $vct_terms_table_name;

            $value = get_option( $this->plugin_name . '_term-page' );
            $user = wp_get_current_user();
            $user_id = $user->ID;
            $vct_terms_table_name = $wpdb->prefix . $this->pretty_name() . '_users';

            $results = $wpdb->get_row("SELECT accepted FROM `$vct_terms_table_name` WHERE user_id='$user_id' ORDER BY `id` DESC;");

            $info = get_option( $this->plugin_name . '_info' );
            $date = get_option( $this->plugin_name . '_term-date' );

            $result = strtotime($results->accepted);
            $cmp = strtotime($date);

            $par1 = '';
            $par2 = '';

            if ($info == '') {
                $par1 = sprintf(__('<p>As of <a class="vct-date">%1$s</a> we have changed our Terms and Conditions</p>', 'vct-terms'), $date);

                $pid = url_to_postid($value);

                if ($pid !== 0) {
                    $excerpt = get_the_excerpt($pid);
                    if ($excerpt !== '') {
                        $par2 = '<p title="' . __('Excerpt') . '">' . $excerpt . '</p>';
                    }
                }
            } else {
                $par2 = '<p title="' . __('Info') . '">' . $info . '</p>';
            }

            if ($result < $cmp && $this->is_term_page()) {
                if (!get_option($this->plugin_name . '_404', false) && is_404()) { return; }
                echo '<div id="vct-terms" class="vct-modal">
                <div class="vct-modal-content">
                <div class="vct-modal-header">
                <h2>' . __('Terms and Conditions', $this->plugin_name) . '</h2>
                </div>' . // -- .modal-header
                '<div class="vct-modal-body">' .
                $par1 .
                $par2 .
                '</div>' . // -- .modal-body
                '<div class="vct-modal-footer">
                <h3>' . sprintf( __('Please accept the new <a href="%s">Terms and Conditions</a>', $this->plugin_name), $value ) .
                '</h3>
                <button id="vct-terms-accept">' . __('Accept', $this->plugin_name) . '</button>
                </div>' . // -- .modal-footer
                '</div>' . // -- .modal-content
                '</div>'; // -- .modal
            }
        }
    }

    private function get_ip() {
        $ip = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    public function accept() {
        global $wpdb;
        global $vct_terms_table_name;

        $vct_terms_table_name = $wpdb->prefix . $this->pretty_name() . '_users';
        $charset_collate = $wpdb->get_charset_collate();
        $user = wp_get_current_user();
        $ip = $this->get_ip();
        $term_date = get_option( $this->plugin_name . '_term-date' );
        $cur_date = current_time( 'mysql' );

        $wpdb->insert(
            $vct_terms_table_name,
            array(
                'user_id'  => $user->ID,
                'accepted' => $term_date,
                'date'     => $cur_date,
                'ip'       => $ip,
            )
        );

        if ($wpdb->last_error) {
            echo "false";
            error_log($wpdb->last_error);
        } else {
            echo "true";
        }

        wp_die();
    }

    public function load_textdomain() {
		$locale = apply_filters( 'plugin_locale', get_locale(), $this->plugin_name );
		load_textdomain( $this->plugin_name, plugin_dir_path( __FILE__ ) . '/languages/' . $locale . '.mo' );
		load_plugin_textdomain( $this->plugin_name, false, plugin_basename( __DIR__ ) . '/languages' );
    }
}

new VcT_Terms();
