# VcT-Terms
A Quick an easy solution to TOC acceptance!

## How to use!
1. Install Plugin
2. Goto general settings
3. Set VcT-Term page to a url linkng to the terms page
4. Set date of current term page
5. Fin

## Customization
vct-terms supports these CSS classes

```css
.vct-modal {} /* The Modal (background) */

.vct-modal-content {} /* Modal Content */


@-webkit-keyframes animatetop {} /* Add Animation */

@keyframes animatetop {} /* Add Animation */

.vct-modal-header {}

.vct-modal-body {}

.vct-modal-body > p .vct-date {}

.vct-modal-footer {}

.vct-modal-footer a {}

.vct-modal-footer a:hover {}

.vct-modal-footer button {}
```

## Installation
### Manual
1. Copy `vct-terms` to `wp-content/plugins`
2. Activate the Plugin in WordPress Plugin menu
3. Go to General Settings

### Automattic
1. Go to the Plugins page
2. Add New then Upload `vct-terms.zip`

### Development

Want to contribute? Great!

#### Building from source
Run `make_zip.(bat|sh)`

## Todos
1. Make Todo List
