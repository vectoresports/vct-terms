@echo off

set DIR=%~dps0
:: remove trailing backslash
if %DIR:~-1%==\ set DIR=%DIR:~0,-1%
set NAME=vct-terms

set SEVENZIPHOME=%PROGRAMFILES%\7-Zip\

"%SEVENZIPHOME%\7z.exe" a -tzip "%DIR%\%NAME%.zip" -xr!thumbs.db -xr!.desktop.ini -xr!project.properties -xr!project.xml -xr!.kpf -xr!php.ini -xr!dwsync.xml -xr!error_log -xr!web.config -xr!.sql -xr!__MACOSX -xr!.lubith -xr!.xcf vct-terms