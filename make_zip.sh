
#!/bin/bash
DIR="$(cd "$("$0")"; pwd -P)"
NAME="vct-terms"

zip -9rq "${DIR}/${NAME}.zip" vct-terms -x \*/.svn\* -x \*/.git\* -x \*/thumbs.db -x \*/desktop.ini -x \*/project.properties -x \*/project.xml -x \*/\*.kpf -x \*/php.ini -x \*/.dwsync.xml -x \*/error_log -x \*/web.config -x \*/\*.sql -x \*/__MACOSX\* -x \*/\*.lubith -x \*/\*.xcf
